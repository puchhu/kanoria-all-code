 #define BLYNK_PRINT Serial    // Comment this out to disable prints and save space
   #include <ESP8266WiFi.h>
   #include <BlynkSimpleEsp8266.h>
//   #include <SimpleTimer.h>
  // char auth[] = "8c9a8ff024404b398948ad6893d1656d";
  char auth[] = "5678431a9e0a4de19e998a3509745c3b";
  char ssid[] = "Kanoria House,4th Floor";
  char pass[] = "9831955404";
//  char ssid[] ="JioFi3_1E1D24";
//  char pass[] ="c3trfzh4jt";
//  char ssid[] = "Ksk";
//  char pass[] = "Ksk0987654321";
  
int blynk_red, blynk_green, blynk_blue;


#include "FastLED.h"
#define NUM_LEDS 120
CRGB leds[NUM_LEDS];
#define PIN 7

BLYNK_WRITE(V1)
{
  blynk_red = param.asInt(); // assigning incoming value from pin V1 to a variable 
}

BLYNK_WRITE(V2)
{
  blynk_green = param.asInt(); // assigning incoming value from pin V1 to a variable 
}

BLYNK_WRITE(V3)
{
  blynk_blue = param.asInt(); // assigning incoming value from pin V1 to a variable 
}


void setup()
{
  Serial.begin(9600);
  Blynk.begin(auth, ssid, pass);
  FastLED.addLeds<WS2811, PIN, GRB>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
}

// *** REPLACE FROM HERE ***
void loop() {
  Blynk.run();
  FadeInOut(blynk_red, blynk_green, blynk_blue); // red
//  FadeInOut(0x7f, 0xff, 0x00); // aquamarine
}

void FadeInOut(byte red, byte green, byte blue){
  float r, g, b;
     
 
    r = red;
    g = green;
    b = blue;
    setAll(r,g,b);
    showStrip();
  

    
 
}

// *** REPLACE TO HERE ***

void showStrip() {


   FastLED.show();

}

void setPixel(int Pixel, byte red, byte green, byte blue) {


   leds[Pixel].r = red;
   leds[Pixel].g = green;
   leds[Pixel].b = blue;

}

void setAll(byte red, byte green, byte blue) {
  for(int i = 0; i < NUM_LEDS; i++ ) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
