#define BLYNK_PRINT Serial    // Comment this out to disable prints and save space 
  #include <ESP8266WiFi.h>
  #include <BlynkSimpleEsp8266.h>
  WidgetBridge bridge1(V70);

  //  char auth[] = "6f0ae2bfd9cc4455bcf3132571c51fef"; ////////////KOUSHIK D3//////////////////////////////
  char auth[] ="505cd57286be4d8fb974ad38f89374c2";/////////FOR TESTING /////////////
//  char auth[] = "046b292955c34216b2d0c5b0c664a836";//////Kanoria Motion Snesor Auth Tokrn////////////
  char ssid[] ="Ksk";
  char pass[] = "Ksk0987654321";
//  char ssid[] ="Nofreenetwork";
//  char pass[]="9831180884";
//  char ssid[] ="JioFi3_1E1D24";
//  char pass[] ="c3trfzh4jt";
//  char ssid[] = "Kanoria House,4th Floor";
//  char pass[] = "9831955404";
  int bathroom_light_state;
  int bathroom_exhaust_state;
  int geyser_led = D7;
  int pir_sensor_input = D0;               // choose the input pin (for PIR sensor)

void setup() {
  Serial.begin(9600);
  Blynk.begin(auth, ssid, pass);
  pinMode(pir_sensor_input, INPUT);     // declare sensor as input
  pinMode(geyser_led,OUTPUT);
}
BLYNK_CONNECTED() {
bridge1.setAuthToken("6f0ae2bfd9cc4455bcf3132571c51fef"); // Token of the hardware B
}

BLYNK_WRITE(V5){
   bathroom_light_state = param.asInt();
   Serial.println(bathroom_light_state);
  Blynk.virtualWrite(V5, param.asInt());
}
BLYNK_WRITE(V7){
  bathroom_exhaust_state = param.asInt();
  Serial.println(bathroom_exhaust_state);
  Blynk.virtualWrite(V7, param.asInt());
}

void loop(){
  Blynk.run();
  if (digitalRead(pir_sensor_input) == HIGH) {            // check if the input is HIGH
        Blynk.notify("T==> Motion detected");  
        if(bathroom_light_state == LOW){
          bridge1.virtualWrite(V5, HIGH);
          Serial.println("BATHROOM LIGHT ON");
        }
        if (bathroom_exhaust_state == LOW){
          bridge1.virtualWrite(V7, HIGH);
          Serial.println("BATHROOM EXHAUST ON");
        }      
    }
  delay(500);
}
