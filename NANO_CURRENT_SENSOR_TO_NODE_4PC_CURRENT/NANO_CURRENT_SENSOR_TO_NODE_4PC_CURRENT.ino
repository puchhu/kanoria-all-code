#define BLYNK_PRINT Serial


#include <SPI.h>
#include <Ethernet.h>
#include <BlynkSimpleEthernet.h>

#include <Adafruit_SSD1306.h>

Adafruit_SSD1306 display(4);


const int LED1 = 2;
const int LED2 = 3;
//const int LED3 = 4;
const int LED4 = 4;


const int sensorIn1 = A0;
const int sensorIn2 = A1;
//const int sensorIn3 = A2;
const int sensorIn4 = A2;

int mVperAmp = 100; // use 100 for 20A Module and 66 for 30A Module

double Voltage_1 = 0;
double VRMS_1 = 0;
double AmpsRMS_1 = 0;

double Voltage_2 = 0;
double VRMS_2 = 0;
double AmpsRMS_2 = 0;

double Voltage_4 = 0;
double VRMS_4 = 0;
double AmpsRMS_4 = 0;
const int currentSensorVccPin_1 = 8;
const int currentSensorVccPin_2 = 9;
const int currentSensorVccPin_3 = 10;
BlynkTimer timer;
void setup(){ 
 Serial.begin(9600);
 pinMode(LED1, OUTPUT);
 pinMode(LED2, OUTPUT);
 //pinMode(LED3, OUTPUT);
 pinMode(LED4, OUTPUT);

 pinMode(currentSensorVccPin_1, OUTPUT);
 pinMode(currentSensorVccPin_2, OUTPUT);
 pinMode(currentSensorVccPin_3, OUTPUT);
 //digitalWrite(D3,HIGH);
     display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
    display.display();
display.clearDisplay();
timer.setInterval(1000L, sensor_1); //  Here you set interval (1sec) and which function to call 
timer.setInterval(1000L, sensor2); //  Here you set interval (1sec) and which function to call 
timer.setInterval(1000L, sensor3); //  Here you set interval (1sec) and which function to call 


}
void sensor_1(){
  digitalWrite(currentSensorVccPin_1,HIGH);
delay(1000);
if(digitalRead(currentSensorVccPin_1)== HIGH){
 Voltage_1 = getVPP1();
 VRMS_1 = (Voltage_1/2.0) *0.707; 
 AmpsRMS_1 = (VRMS_1 * 1000)/mVperAmp;
 Serial.print(AmpsRMS_1);
 Serial.println(" Amps RMS 1");
 
 if(AmpsRMS_1< 0.39){
  Serial.println("Light OFF");
  digitalWrite(LED1,LOW);
 }
 else{
  Serial.println("Light ON");
  digitalWrite(LED1,HIGH);
 }
display.setTextColor(WHITE);
display.setCursor(10,10);
display.println(AmpsRMS_1);
display.display();
//delay(2000);
 digitalWrite(currentSensorVccPin_1,LOW);
 delay(3000);

}
else{
  Serial.println("SENSOR 1 DEACTIVATE");
}

}
void sensor2(){
digitalWrite(currentSensorVccPin_2,HIGH);
delay(1000);
if(digitalRead(currentSensorVccPin_2)== HIGH){

 Voltage_2 = getVPP2();
 VRMS_2 = (Voltage_2/2.0) *0.707; 
 AmpsRMS_2 = (VRMS_2 * 1000)/mVperAmp;
 Serial.print(AmpsRMS_2);
 Serial.println(" Amps RMS 2");
 if(AmpsRMS_2< 0.20){
  Serial.println("Light OFF");
  digitalWrite(LED2,LOW);
 }
 else{
  Serial.println("Light ON");
  digitalWrite(LED2,HIGH);
 }
display.setTextColor(WHITE);

display.setCursor(40,10);
display.println(AmpsRMS_2);
display.display();
//delay(2000);
 digitalWrite(currentSensorVccPin_2,LOW);
 delay(3000);

}
 else{
  Serial.println("SENSOR  2 DEACTIVATE");
  
 }
  
}
void sensor3(){
digitalWrite(currentSensorVccPin_3,HIGH);
delay(1000);
if(digitalRead(currentSensorVccPin_3)== HIGH){

 Voltage_4 = getVPP4();
 VRMS_4 = (Voltage_4/2.0) *0.707; 
 AmpsRMS_4 = (VRMS_4 * 1000)/100;
 Serial.print(AmpsRMS_4);
 Serial.println(" Amps RMS 4");
 if(AmpsRMS_4< 0.23){
  Serial.println("Light OFF");
  digitalWrite(LED4,LOW);
 }
 else{
  Serial.println("Light ON");
  digitalWrite(LED4,HIGH);
 }
display.setTextColor(WHITE);
display.setCursor(10,20);
display.println(AmpsRMS_4);
 
display.display();
//delay(2000);
 digitalWrite(currentSensorVccPin_3,LOW);
delay(3000);  
}
 else{
  Serial.println("SENSOR 3 DEACTIVATE");
  
 }
  
}
void loop(){
/////////////////////////
//delay(500);
////////////
// Voltage = getVPP3();
// VRMS = (Voltage/2.0) *0.707; 
// AmpsRMS = (VRMS * 1000)/mVperAmp;
// Serial.print(AmpsRMS);
// Serial.println(" Amps RMS 3");
// if(AmpsRMS< 0.06){
//  Serial.println("Light OFF");
//  digitalWrite(LED3,LOW);
// }
// else{
//  Serial.println("Light ON");
//  digitalWrite(LED3,HIGH);
// }
///////////
//delay(500);
/////////
// Voltage = getVPP();
// VRMS = (Voltage/2.0) *0.707; 
// AmpsRMS = (VRMS * 1000)/mVperAmp;
// Serial.print(AmpsRMS);
// Serial.println(" Amps RMS");
// if(AmpsRMS< 0.06){
//  Serial.println("Light OFF");
//  digitalWrite(LED,LOW);
// }
// else{
//  Serial.println("Light ON");
//  digitalWrite(LED,HIGH);
// }
display.clearDisplay();
timer.run();
}

float getVPP1()
{
  float result_1;
  
  int readValue_1;             //value read from the sensor
  int maxValue_1 = 0;          // store max value here
  int minValue_1 = 1024;          // store min value here
  
   uint32_t start_time_1 = millis();
   while((millis()-start_time_1) < 800) //sample for 1 Sec
   {
       readValue_1 = analogRead(sensorIn1);
       // see if you have a new maxValue
       if (readValue_1 > maxValue_1) 
       {
           /*record the maximum sensor value*/
           maxValue_1 = readValue_1;
       }
       if (readValue_1 < minValue_1) 
       {
           /*record the maximum sensor value*/
           minValue_1 = readValue_1;
       }
   }
   
   // Subtract min from max
   result_1 = ((maxValue_1 - minValue_1) * 5.0)/1024.0;
      
   return result_1;
 }
 //////////////////////////////
 float getVPP2()
{
  float result_2;
  
  int readValue_2;             //value read from the sensor
  int maxValue_2 = 0;          // store max value here
  int minValue_2 = 1024;          // store min value here
  
   uint32_t start_time_2 = millis();
   while((millis()-start_time_2) < 800) //sample for 1 Sec
   {
       readValue_2 = analogRead(sensorIn2);
       // see if you have a new maxValue
       if (readValue_2 > maxValue_2) 
       {
           /*record the maximum sensor value*/
           maxValue_2 = readValue_2;
       }
       if (readValue_2 < minValue_2) 
       {
           /*record the maximum sensor value*/
           minValue_2 = readValue_2;
       }
   }
   
   // Subtract min from max
   result_2 = ((maxValue_2 - minValue_2) * 5.0)/1024.0;
      
   return result_2;
 }
 
//float getVPP3()
//{
//  float result;
//  
//  int readValue;             //value read from the sensor
//  int maxValue = 0;          // store max value here
//  int minValue = 1024;          // store min value here
//  
//   uint32_t start_time = millis();
//   while((millis()-start_time) < 1000) //sample for 1 Sec
//   {
//       readValue = analogRead(sensorIn3);
//       // see if you have a new maxValue
//       if (readValue > maxValue) 
//       {
//           /*record the maximum sensor value*/
//           maxValue = readValue;
//       }
//       if (readValue < minValue) 
//       {
//           /*record the maximum sensor value*/
//           minValue = readValue;
//       }
//   }
//   
//   // Subtract min from max
//   result = ((maxValue - minValue) * 5.0)/1024.0;
//      
//   return result;
// }
////////////////////////////////////// 
float getVPP4()
{
  float result_4;
  
  int readValue_4;             //value read from the sensor
  int maxValue_4 = 0;          // store max value here
  int minValue_4 = 1024;          // store min value here
  
   uint32_t start_time_4 = millis();
   while((millis()-start_time_4) < 800) //sample for 1 Sec
   {
       readValue_4 = analogRead(sensorIn4);
       // see if you have a new maxValue
       if (readValue_4 > maxValue_4) 
       {
           /*record the maximum sensor value*/
           maxValue_4 = readValue_4;
       }
       if (readValue_4 < minValue_4) 
       {
           /*record the maximum sensor value*/
           minValue_4 = readValue_4;
       }
   }
   
   // Subtract min from max
   result_4 = ((maxValue_4 - minValue_4) * 5.0)/1024.0;
      
   return result_4;
 }
