 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 ////////////////////////////////////////////////KANORIA HOUSE 4TH FLOOR D1//////////////////////
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
  #define BLYNK_PRINT Serial    // Comment this out to disable prints and save space
  #include <ESP8266WiFi.h>
  #include <BlynkSimpleEsp8266.h>
  #include <SimpleTimer.h>

// char auth[] = "8c9a8ff024404b398948ad6893d1656d";////////////////////KOUSHIK////////////////////////
//  char auth[] = "fb2b2c29e28747f0b214f3b2ce3713da";//////////////////////AYUSH SIR/////////////////////
 char auth[] = "eb10ea81879142599427dad822dd8cb8";/////////////*BLYNK PROJECT NAME*/"Kanoria House D1"////////////////////
 //char ssid[] = "Ksk";
 //char pass[] = "Ksk0987654321";
 char ssid[] = "Kanoria House,4th Floor";//////////////////ONLY 4th FLOOR///////////////////////////
 char pass[] = "9831955404";
  const int sensorIn = A0;
  int mVperAmp = 66; // use 100 for 20A Module and 66 for 30A Module
  const int R1  = 14;            // Output Relay 1 GPI0 14 (D5)
  //const int R2  = 4;

  double result;
  double VRMS = 0;
  double AmpsRMS = 0;

  SimpleTimer timer;
  void getVPP()
  {
     int readValue;             //value read from the sensor
    int maxValue = 0;          // store max value here
    int minValue = 1024;          // store min value here
    
     uint32_t start_time = millis();
     while((millis()-start_time) < 200) //sample for 1 Sec
     {
         readValue = analogRead(sensorIn);
         // see if you have a new maxValue
         if (readValue > maxValue)
         {
             /*record the maximum sensor value*/
             maxValue = readValue;
         }
         if (readValue < minValue) 
         {
             /*record the maximum sensor value*/
             minValue = readValue;
         }
     }
     
     // Subtract min from max
     result = ((maxValue - minValue) * 5.0)/1024.0;
        
    VRMS = (result/2.0) *0.707; 
   AmpsRMS = (VRMS * 1000)/mVperAmp;
   Serial.print(AmpsRMS);
   Serial.println(" Amps RMS");
   Blynk.virtualWrite(7,AmpsRMS);
   if (AmpsRMS < 0.5)
  {
    Blynk.virtualWrite(8,"L1 OFF");
    //Serial.println("HELLO..off");
  }
  else
  {
    Blynk.virtualWrite(8,"L1 ON");
   // Serial.println("HELLO..on");  
  }
    
}

 
//Serial.println("HELLO..!");
/////////////////////////////////////////////////////////////FOR WIFI DISCONNECT////////////////////////////////////////////////
BLYNK_CONNECTED(){Blynk.syncVirtual(V1);}
BLYNK_WRITE(V1){ int buttonState = param.asInt(); }
//////////////////////////////////////////////////////////////////////////KANORIA HOUSE 4TH FLOOR D1 PIN HIGH WITH CURRENT SENSOR//////////////////////////////
  void setup(){ 
   Serial.begin(9600);
   Blynk.begin(auth, ssid, pass);
   pinMode(R1,OUTPUT);
   //Serial.println("HELLO..SETUP");
    timer.setInterval(1000, getVPP);

    int readValue;             //value read from the sensor
    int maxValue = 0;          // store max value here
    int minValue = 1024;          // store min value here
    
     uint32_t start_time = millis();
     while((millis()-start_time) < 200) //sample for 1 Sec
     {
         readValue = analogRead(sensorIn);
         // see if you have a new maxValue
         if (readValue > maxValue)
         {
             /*record the maximum sensor value*/
             maxValue = readValue;
         }
         if (readValue < minValue) 
         {
             /*record the maximum sensor value*/
             minValue = readValue;
         }
     }
     
     // Subtract min from max
     result = ((maxValue - minValue) * 5.0)/1024.0;
        
    VRMS = (result/2.0) *0.707; 
   AmpsRMS = (VRMS * 1000)/mVperAmp;

//Serial.println(AmpsRMS);
//Serial.println("---------");
if (AmpsRMS < 0.5)
  {
  }
  else
  {
  Serial.println(AmpsRMS);
//  Serial.println("HELLO..REST..");  
  digitalWrite(R1, 1);   
  }
//    pinMode(R2,OUTPUT);
  }
void loop(){
Blynk.run();

timer.run();
}
