/* 
* PIR sensor tester
*/
  #define BLYNK_PRINT Serial    // Comment this out to disable prints and save space
  #include <ESP8266WiFi.h>
  #include <BlynkSimpleEsp8266.h>
  WidgetBridge bridge1(V70);

  int ledPin = D2;                // choose the pin for the LED
  int bathroom_light_switch = D1;
  int bathroom_exhaust_fan_switch = D3;
//  int bathroom_light_state = D5;
//  int bathroom_exhaust_state = D8;
  int geyser_state = D7;
  int input_Pir_Sensor_Pin = D0;               // choose the input pin (for PIR sensor)
  int pirState = LOW;             // we start, assuming no motion detected
  int inputPinState = 0;                    // variable for reading the pin status
  //char auth[] = "644d924dcd85461eb870ce13aef4e614"; ////////////KOUSHIK//////////////////////////////
  char auth[] = "c2c1218666f44309be0e3cb751ca7676";//////AYUSH////////////
//  char ssid[] ="Ksk";
//  char pass[] = "Ksk0987654321";
//  char ssid[] ="Nofreenetwork";
//  char pass[]="9831180884";
  char ssid[] ="JioFi3_1E1D24";
  char pass[] ="c3trfzh4jt";
//  char ssid[] = "Kanoria House,4th Floor";
//  char pass[] = "9831955404";

  int time_lapsed_from_last_motion_detected;
 
void setup() {
  Serial.begin(9600);
  Blynk.begin(auth, ssid, pass);
  //pinMode(ledPin, OUTPUT);      // declare LED as output
  //pinMode(input_Pir_Sensor_Pin, INPUT);     // declare sensor as input
  pinMode(geyser_state,OUTPUT);
  //pinMode(bathroom_light_switch,OUTPUT);
  
}
BLYNK_CONNECTED() {
bridge1.setAuthToken("c2c1218666f44309be0e3cb751ca7676"); // Token of the hardware B
}  
 
void loop(){
  Blynk.run();
 
  inputPinState = digitalRead(input_Pir_Sensor_Pin);  // read input value
  Serial.println("--------------------------------");
  Serial.println(inputPinState);
  Serial.println("------");
  Serial.println(pirState);

  if (inputPinState == HIGH) {            // check if the input is HIGH
   
    time_lapsed_from_last_motion_detected=0;
     
    if (pirState == LOW) {
      // we have just turned on
      Serial.println("Motion detected!");
      // We only want to print on the output change, not state
      pirState = HIGH;
      digitalWrite(ledPin, HIGH);  // turn LED ON
        if(digitalRead(bathroom_light_switch) == LOW){
//          bridge1.digitalWrite(bathroom_light_switch, HIGH);
        }
        if(digitalRead(bathroom_exhaust_fan_switch) == LOW){
//          bridge1.digitalWrite(bathroom_exhaust_fan_switch, HIGH);
        }
     }
  }
  else {
    time_lapsed_from_last_motion_detected+=500;
     Serial.println("*********************************");
     Serial.println(time_lapsed_from_last_motion_detected);

    if (pirState == HIGH){
      if(time_lapsed_from_last_motion_detected > 1000)
      {// we have just turned of
      Serial.println("Motion ended!");
      Serial.println(time_lapsed_from_last_motion_detected);
      // We only want to print on the output change, not state
      pirState = LOW;
      digitalWrite(ledPin, LOW); // turn LED OFF
//      digitalWrite(bathroom_light_switch,LOW);
//      digitalWrite(bathroom_exhaust_fan_switch,LOW);
//      bridge1.digitalWrite(bathroom_light_switch, LOW);
//      bridge1.digitalWrite(bathroom_exhaust_fan_switch, LOW);
     // bridge1.virtualWrite(bathroom_light_switch,LOW)
      Serial.println("WE OFF");
      }
    }
  }
  delay(500);
}
